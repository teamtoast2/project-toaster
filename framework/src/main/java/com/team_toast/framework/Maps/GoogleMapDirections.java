package com.team_toast.framework.Maps;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

/**
 * Class to handle request and generation of direction data for GoogleMaps
 */
public class GoogleMapDirections extends AsyncTask<Double, Void, String>{

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";


    public GoogleMapDirections() {
    }

    @Override
    protected String doInBackground(Double... params) {
        return getJSONFromUrl(params[0], params[1], params[2], params[3]);
    }

    /**
     * Retrieve a JSON data object by requesting it from Google using a generated URL containing
     * start location and target destination latitudes and longitudes.
     * @param sourcelat start latitude
     * @param sourcelog start longitude
     * @param destlat target latitude
     * @param destlog target longitude
     * @return String representation of JSON object
     */
    public String getJSONFromUrl(double sourcelat, double sourcelog, double destlat, double destlog) {
        //Retrieve URL to query google with
        String url = makeURL (sourcelat, sourcelog, destlat, destlog);
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            json = sb.toString();
            is.close();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return json;
    }

    /**
     * Generate the URL to query Google with. Contains start location and target destination
     * @param sourcelat start latitude
     * @param sourcelog start longitude
     * @param destlat target latitude
     * @param destlog target longitude
     * @return String representation of URL query
     */
    public String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString( destlog));
        urlString.append("&sensor=false&mode=walking&alternatives=true");
        urlString.append("&key=AIzaSyCI84NQq8mKr6ojMiYtxv2lBgRiPr-LAME");
        return urlString.toString();
    }

    /**
     * Begin the AsyncTask to query Google for directions data
     * @param sourcelat start latitude
     * @param sourcelog start longitude
     * @param destlat target latitude
     * @param destlog target longitude
     * @return String reprentation of JSON object retrieved from Google
     */
    public String getGoogleDirections(double sourcelat, double sourcelog, double destlat, double destlog){
        this.execute(sourcelat, sourcelog, destlat, destlog);
        try {
            json =  this.get();
        } catch (InterruptedException e) {
            Log.e("getGoogleDirections: ", "InterruptedException Occured");
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.e("getGoogleDirections: ", "ExecutionException Occured");
            e.printStackTrace();
        }

        return json;
    }

}
