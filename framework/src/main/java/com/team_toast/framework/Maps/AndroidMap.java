package com.team_toast.framework.Maps;

import android.graphics.Color;
import android.os.Handler;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.team_toast.framework.Location.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to handle map based functionality
 */
public class AndroidMap implements OnMapReadyCallback {

    /**
     * Instance of itself
     */
    private static AndroidMap instance;

    private GoogleMap map;
    private GoogleMapDirections googleDirections;
    private Handler handler;
    private LatLng startLatLng;

    public static Location staticTargetDestination;
    public static boolean  isThreadStarted;

    private AndroidMap(){
        this.isThreadStarted = false;
    }

    /**
     * Returns an instance of the AndroidMap
     * @return
     */
    public static AndroidMap getInstance()
    {
        //If instance is NULL create new instance
        if(instance == null) {
            instance = new AndroidMap();
        }

        return instance;
    }

    /**
     * Initalises the class variables.
     *
     * @param mapFragment   The fragment inwhich the map will be displayed
     * @param map           The google map
     */
    public void initialise(SupportMapFragment mapFragment, GoogleMap map) {
        mapFragment.getMapAsync(this);
        this.map = map;
        this.map.setMyLocationEnabled(true);
        handler=new Handler();
    }

    /**
     * Sets the Maps default position
     * @param loc   Latitute and Longitude object
     */
    public void setMapDefaultPosition(LatLng loc)
    {
        this.startLatLng = loc;
    }

    /**
     * GoogleMap accessor method
     * @return  The GoogleMap
     */
    public GoogleMap getMap() {
        return map;
    }

    /**
     * Converts a String into a JSON object and uses this object to generate a map route.
     * @param result String representation of JSON route data
     */
    public void drawPath(String  result) {
        try {
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            Polyline line = map.addPolyline(new PolylineOptions()
                            .addAll(list)
                            .width(12)
                            .color(Color.parseColor("#05b1fb"))//Google maps blue color
                            .geodesic(true)
            );
        }
        catch (JSONException e) {
        }
    }

    /**
     * Creates a list of LatLng for route data that is used to generate a Polyline.
     * @param encoded String data to create the LatLng list
     * @return A list of LatLng objects
     */
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 15));
    }

    /**
     * Accessor method
     * @return GoogleMap object
     */
    public GoogleMap getGoogleMap(){
        return map;
    }

    /**
     * Method called to initially start the route update thread, but also each time a new route is
     * requested to update the target destination.
     * @param deviceLocation The current location of the device
     * @param targetDestination The destination to route to
     * @param interval The interval to update the route data within the update thread
     */
    public void updateRoute(final Location deviceLocation, final Location targetDestination, final int interval){

        //Thread that redraws directions on a set time interval
        final Runnable updateTask=new Runnable() {
            @Override
            public void run() {
                drawDirections(deviceLocation);
                handler.postDelayed(this, interval);
            }
        };

        //Update the target destination
        AndroidMap.staticTargetDestination = targetDestination;
        //draw the directions on the map
        drawDirections(deviceLocation);
        //If first call, begin the thread
        if(!AndroidMap.isThreadStarted) {
            handler.postDelayed(updateTask, 1000);
            AndroidMap.isThreadStarted = true;
        }
    }

    /**
     * Update the devices location, clear the current route on the map and draw a new updated one.
     * @param deviceLocation The devices location object
     */
    public void drawDirections(final Location deviceLocation){
        //Update the devices location
        deviceLocation.updateToLastKnownLocation();
        googleDirections = new GoogleMapDirections();
        String directions = null;
        //If LatLng is not null, retrieve the directions to target destination
        if(deviceLocation.getLatLng() != null) {
            directions = googleDirections.getGoogleDirections(deviceLocation.getLatLng().latitude,
                    deviceLocation.getLatLng().longitude, staticTargetDestination.getLatLng().latitude,
                    staticTargetDestination.getLatLng().longitude);
        }
        map.clear();
        //If directions are not null, draw the new route
        if(directions != null) {
            AndroidMap.this.drawPath(directions);
        }
    }

    /**
     * Add a marker to the map
     * @param loc location to place the marker
     * @param markerName Name of the marker
     */
    public void addMarker(Location loc, String markerName){
        this.map.addMarker(new MarkerOptions()
                .position(new LatLng(loc.getLatLng().latitude, loc.getLatLng().longitude))
                .title(markerName));

    }

}