package com.team_toast.framework.FileIO;

/**
 * AltHTMLStrategy, used to display the strategy string as a html string.
 */
public class AltHTMLStrategy extends FormatStrategy {
    @Override
    public String Format(String Heading) {
        return "<HTML>\n<HEAD>\n<TITLE>Test Title</TITLE>\n\n<BODY>\n<H1>"
                + Heading + "</H1>\n\n\n</BODY>\n</HTML>";
    }
}