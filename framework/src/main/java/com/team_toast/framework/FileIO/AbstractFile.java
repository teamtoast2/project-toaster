package com.team_toast.framework.FileIO;

/**
 * Abstract file, used in the strategy pattern for saving files. Files extending this are able to
 * be given to the file bridge and saved to file.
 */
public abstract class AbstractFile {
    private FileImplementation fileImp;
    protected AbstractFile(FileImplementation imp) {
        fileImp = imp;
    }
    protected final String read(String fileName) {
        return fileImp.read(fileName);
    }
    protected final boolean write(String FileName, String contents) {
        return fileImp.write(FileName, contents);
    }
    public void setImplementation(FileImplementation newImp) {
        fileImp = newImp;
    }
}