package com.team_toast.framework.FileIO;

/**
 *
 */
public abstract class FormatStrategy {
    public abstract String Format(String Body);
}