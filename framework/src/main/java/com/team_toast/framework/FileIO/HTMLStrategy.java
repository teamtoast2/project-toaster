package com.team_toast.framework.FileIO;

/**
 * Created by qrk13cdu on 12/01/2016.
 */
public class HTMLStrategy extends FormatStrategy {
    @Override
    public String Format(String Body) {
        return "<HTML>\n<HEAD>\n<TITLE></TITLE>\n\n<BODY>\n" + Body + "\n</BODY>\n</HTML>";
    }
}