package com.team_toast.framework.FileIO;

import android.app.Activity;

/**
 * Created by qrk13cdu on 12/01/2016.
 */
public abstract class FileImplementation {
    protected Activity activity;
    protected FileImplementation(Activity activity) {
        this.activity = activity;
    }
    public abstract String read(String fileName);
    public abstract boolean write(String fileName, String content);
}
