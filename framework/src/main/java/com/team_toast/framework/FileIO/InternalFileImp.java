package com.team_toast.framework.FileIO;

import android.app.Activity;
import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
* ExternalFileImp extends FileImplementation, handles the saving of files to disk.
 */
public class InternalFileImp extends FileImplementation {
    public InternalFileImp(Activity TheAct) {
        super(TheAct);
    }
    @Override
    public String read(String fileName) {
        FileInputStream fis;
        try {
            fis = activity.openFileInput(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "ERROR_FILE_NOT_FOUND";
        }
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader bufferedReader = new BufferedReader(isr);
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR_WHILE_READING_FILE";
        }
    }
    @Override
    public boolean write(String fileName, String content) {
        FileOutputStream fos;
        try {
            fos = activity.openFileOutput(fileName, Context.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        try {
            fos.write(content.getBytes());
            fos.close();
            return true;
        } catch (IOException e) {e.printStackTrace();
            return false;
        }
    }
}