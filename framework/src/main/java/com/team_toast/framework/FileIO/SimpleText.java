package com.team_toast.framework.FileIO;

/**
 * Strategy, anything that extends this is able to then be used with a fileImplementation to save
 * files to file.
 */
public class SimpleText extends AbstractFile{
    //private String Heading = "";
    private String Body = "";
    private FormatStrategy MyStrat = new StringStrategy();
    public SimpleText(FileImplementation Imp) {
        super(Imp);
    }
    public void setMyStrat(FormatStrategy myStrat) {
        MyStrat = myStrat;
    }
    public void setBody(String body) {
        Body = body;
    }
    public String formatOutput() {return MyStrat.Format(Body);}
    public boolean writeToFile(String FileName) {
        return write(FileName, formatOutput());
    }
    public boolean readFromFile(String FileName) {
        String FileContents = read(FileName);
        Body = FileContents;
        return !FileContents.contains("ERROR_");
    }
}