package com.team_toast.framework.DataInterchange;

import android.content.Context;

import com.team_toast.framework.DataInterchange.DataInterchange;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Android implementation to define parsing of XML files
 */
public abstract class AndroidXmlParser implements DataInterchange{

    protected Context appContext;
    protected XmlPullParser parser;

    public AndroidXmlParser(Context appContext){
        this.appContext = appContext;
    }

    /**
     * Loads a XML and parses it into a parser object for processing.
     * @param fileName Name of file to load
     * @throws XmlPullParserException
     * @throws IOException
     */
    public void loadFile(String fileName) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        parser = xmlFactoryObject.newPullParser();

        InputStream in_s = appContext.getAssets().open(fileName);
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in_s, null);

        //This method is defined in a subclass within the application to provide specific parsing.
        parseData();
    }



}
