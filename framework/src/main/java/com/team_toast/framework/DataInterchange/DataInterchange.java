package com.team_toast.framework.DataInterchange;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Interface to define parsing methods, uses strategy pattern. Intended to be implemented for
 * parsing such as JSON or XML.
 */
public interface DataInterchange {

    public void loadFile(String fileName) throws XmlPullParserException, IOException;

    public void parseData() throws XmlPullParserException, IOException;
}
