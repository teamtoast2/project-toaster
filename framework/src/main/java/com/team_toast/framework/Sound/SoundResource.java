package com.team_toast.framework.Sound;

import android.app.Activity;

/**
 * Sound Resource
 *@author qrk13cdu
 *@since 2015-10-14
 *@version 1.0
 * Plays a sound resource by providing a handle to the audio resource. Used to play any sound
 * effect e.g. clicking a button.
 */
public class SoundResource {
    private Audio myAudio;
    private Sound mySound;

    /**
     * Creates a SoundResource Object by parsing an activity to the inbuilt AndroidAudio method.
     * @see AndroidAudio
     * @param act Activity of which requires a SoundResource.
     */
    public SoundResource(Activity act){
        myAudio = new AndroidAudio(act);
    }

    /**
     * Loads audio file to be used in the SoundResource.
     * @param resourcePath A path into the local assets folder. Used to locate the audio file.
     * @see Audio
     * @see Sound
     */
    public void load(String resourcePath){
        mySound = myAudio.createSound(resourcePath);
    }

    /**
     * Plays loaded Sound, currently fixed volume of 0.9.
     * Functionality supplied by Sound class in framework.
     * @see Sound
     */
    public void play(){
        mySound.play(0.9f);
    }

    /**
     * Stops the current playing Sound.
     * Functionality supplied by Sound class in framework.
     * @see Sound
     */
    public void stop(){
        mySound.stop();
    }
}
