package com.team_toast.framework.Sound;

import android.media.SoundPool;

import com.team_toast.framework.Sound.Sound;

/**
 * Manages the SoundPool. Implements Interface Sound.
 * Contains an member int to identify a sound and a SoundPool.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see SoundPool
 */
public class AndroidSound implements Sound {
    int soundId;
    SoundPool soundPool;

    /**
     * Creates a AndroidSound Object.
     * @param soundPool SoundPool reference passed and assigned to member variable.
     * @param soundId int identifier into the SoundPool.
     * @see SoundPool
     */
    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    /**
     * Plays the Sound in the SoundPool.
     * @param volume Float representation of the volume for left and right.
     * @see SoundPool
     */
    @Override
    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    /**
     * Removes the current Sound from the SoundPool.
     * @see SoundPool
     */
    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }

    @Override
    public void stop() {
        soundPool.stop(soundId);
    }

}
