package com.team_toast.framework.Sound;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;

import com.team_toast.framework.Sound.Music;
/**
 * Manages and creates AndroidMusic. Implements Music, OnCompletionListener,
 * OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener.
 * Contains a MediaPlayer.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 * @see MediaPlayer
 * @see Music
 * @see OnCompletionListener
 * @see OnSeekCompleteListener
 * @see OnPreparedListener
 * @see OnVideoSizeChangedListener
 */
public class AndroidMusic implements Music, OnCompletionListener, OnSeekCompleteListener, OnPreparedListener, OnVideoSizeChangedListener {
    MediaPlayer mediaPlayer;
    boolean isPrepared = false;

    /**
     * Creates an AndroidMusic Object. Initialises the member MediaPlayer, setting the data source
     * and OnCompletion, OnSeekComplete, OnPrepared and OnVideoSizeChanged Listeners.
     * @see MediaPlayer
     * @param assetDescriptor
     */
    public AndroidMusic(AssetFileDescriptor assetDescriptor) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(),
                    assetDescriptor.getStartOffset(),
                    assetDescriptor.getLength());
            mediaPlayer.prepare();
            isPrepared = true;
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnSeekCompleteListener(this);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnVideoSizeChangedListener(this);
            
        } catch (Exception e) {
            throw new RuntimeException("Couldn't load music");
        }
    }

    /**
     * Disposes the current stored MediaPlayer.
     * @see MediaPlayer
     */
    @Override
    public void dispose() {
    
         if (this.mediaPlayer.isPlaying()){
               this.mediaPlayer.stop();
                }
        this.mediaPlayer.release();
    }

    /**
     * Checks whether the MediaPlayer is looping or non-looping.
     * @return Boolean true if looping, false if not.
     * @see MediaPlayer
     */
    @Override
    public boolean isLooping() {
        return mediaPlayer.isLooping();
    }

    /**
     * Checks whether the MediaPlayer is playing.
     * @return Boolean true if playing, false if not.
     * @see MediaPlayer
     */
    @Override
    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    /**
     * Checks whether the MediaPlayer is stopped and prepared.
     * @return Boolean true if stopped, false if not.
     * @see MediaPlayer
     */
    @Override
    public boolean isStopped() {
        return !isPrepared;
    }

    /**
     * Pauses playback of MediaPlayer.
     * @see MediaPlayer
     */
    @Override
    public void pause() {
        if (this.mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    /**
     * Plays playback. Checks if playing back already, if not checks if it
     * is prepared, prepares if not, then starts the playback of the MediaPlayer.
     * @see MediaPlayer
     */
    @Override
    public void play() {
        if (this.mediaPlayer.isPlaying())
            return;

        try {
            synchronized (this) {
                if (!isPrepared)
                    mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the MediaPlayer to be looping or non-looping.
     * @param isLooping Boolean whether to loop or not
     * @see MediaPlayer
     */
    @Override
    public void setLooping(boolean isLooping) {
        mediaPlayer.setLooping(isLooping);
    }

    /**
     * Sets the volume on this MediaPlayer.
     * @param volume Float representation of volume of both left and right.
     * @see MediaPlayer
     */
    @Override
    public void setVolume(float volume) {
        mediaPlayer.setVolume(volume, volume);
    }

    /**
     * Stops playback. Checks if playing back already, if true, stops the MediaPlayer. Sets prepared
     * to false.
     * @see MediaPlayer
     */
    @Override
    public void stop() {
         if (this.mediaPlayer.isPlaying() == true){
        this.mediaPlayer.stop();
        
       synchronized (this) {
           isPrepared = false;
        }}
    }

    /**
     * On call, this synchronises on itself and sets prepared to false.
     * @param player
     */
    @Override
    public void onCompletion(MediaPlayer player) {
        synchronized (this) {
            isPrepared = false;
        }
    }

    /**
     * Seeks to specified time position. In this case, value of 0 is default.
     */
    @Override
    public void seekBegin() {
        mediaPlayer.seekTo(0);
        
    }

    /**
     * On call, this synchronises on itself and sets prepared to true.
     * @param player
     */
    @Override
    public void onPrepared(MediaPlayer player) {
        // TODO Auto-generated method stub
         synchronized (this) {
               isPrepared = true;
            }
        
    }

    /**
     * Incomplete implementation, empty method body.
     * @param player
     */
    @Override
    public void onSeekComplete(MediaPlayer player) {
        // TODO Auto-generated method stub
        
    }

    /**
     * Incomplete implementation, empty method body.
     * @param player
     * @param width
     * @param height
     */
    @Override
    public void onVideoSizeChanged(MediaPlayer player, int width, int height) {
        // TODO Auto-generated method stub
        
    }
}
