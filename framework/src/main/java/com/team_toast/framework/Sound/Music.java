package com.team_toast.framework.Sound;
/**
 * Interface, Manages and creates AndroidMusic. See implementation {@link AndroidMusic}
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public interface Music {
    public void play();

    public void stop();

    public void pause();

    public void setLooping(boolean looping);

    public void setVolume(float volume);

    public boolean isPlaying();

    public boolean isStopped();

    public boolean isLooping();

    public void dispose();

    void seekBegin();
}