package com.team_toast.framework.Sound;

/**
 * Sound interface, provides method headers for the playing and disposing of a Sound. See implementation in {@link com.team_toast.project_toaster.implementation.AndroidSound}.
 * @author Joe Lilley
 * @since 2015-10-22
 * @version 1.0
 */
public interface Sound {
    public void play(float volume);

    public void dispose();

    public void stop();
}
