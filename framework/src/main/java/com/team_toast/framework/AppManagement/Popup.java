package com.team_toast.framework.AppManagement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class Popup {


    public void newPopup(Context con, String title, String message){
        new AlertDialog.Builder(con)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }})
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
