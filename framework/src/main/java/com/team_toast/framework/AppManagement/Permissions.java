package com.team_toast.framework.AppManagement;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by akc13dyu on 11/01/2016.
 */
public class Permissions {


    public Permissions(){}


    public void requestFinePermissions(Context currentContext) {

        if ( ContextCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) currentContext, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    public void requestCoarsePermissions(Context currentContext) {
        if (ContextCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( (Activity)currentContext, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 1 );
        }
    }

    public static boolean isLocationPermissionsGranted(Context currentContext){
        if( ContextCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(currentContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Popup p = new Popup();
            p.newPopup(currentContext, "Error.", "This functionality requires location permissions!");
            Permissions permissions = new Permissions();
            permissions.requestFinePermissions(currentContext);
            permissions.requestCoarsePermissions(currentContext);
            return false;
        }
        return true;
    }

    public void requestInternalFileWritePermissions(Context currentContext) {

    }

    public static void requestExternalFileWritePermissions(Context currentContext) {

    }


}

