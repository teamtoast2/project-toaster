package com.team_toast.framework.AppManagement;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class Notification {

    public Notification() {}

    public void pushNotification(Context context, int icon, CharSequence title, CharSequence text, Intent intent) {

        //Create pending Intent
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Initialise notification and set icon, title, text, and pendingIntent
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(icon);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(text);
        mBuilder.setContentIntent(pendingIntent);

        //notification ID
        int mNotificationId = 001;

        //gets instance of NotificationManager Service
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        //Build and issue notification
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }
}
