package com.team_toast.framework.StateManager;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class Memento {

    private MementoState state;

    public Memento(MementoState state) {
        this.state = state;
    }

    public MementoState getState() {
        return state;
    }
}
