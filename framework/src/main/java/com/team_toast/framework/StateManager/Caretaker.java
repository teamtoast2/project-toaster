package com.team_toast.framework.StateManager;

import java.util.ArrayList;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class Caretaker {

    private ArrayList<Memento> savedStates = new ArrayList<>();

    public void addMemento(Memento m) {
        savedStates.add(m);
    }

    public Memento getMemento() {

        Memento m = null;

        //If there are states saved, delete and return the last element
        if(savedStates.size() > 0) {
            m = savedStates.get(savedStates.size() - 1);
            savedStates.remove(m);
        }

        return m;
    }
}
