package com.team_toast.framework.StateManager;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class Originator {

    protected MementoState state;

    public final void setState(MementoState state)
    {
        this.state = state;
    }

    public final Memento saveToMemento() {
        return new Memento(state);
    }

    public final void restoreFromMemento(Memento m) {

        if(m == null) {
            state = null;
        }
        else {
            state = m.getState();
        }
    }
}
