package com.team_toast.framework.StateManager;

//Framework imports
import com.team_toast.framework.StateManager.MementoState;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class StringState extends MementoState {

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
};
