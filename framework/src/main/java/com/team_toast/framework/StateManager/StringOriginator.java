package com.team_toast.framework.StateManager;

/**
 * Created by akc13dyu on 12/01/2016.
 */
public class StringOriginator  extends Originator {

    public String getMyValue() {
        if (state instanceof StringState) {
            return ((StringState)state).getState();
        } else {
            return "";
        }
    }
    public void setMyValue(String myValue) {
        StringState newState = new StringState();
        newState.setState(myValue);
        state = newState;
    }
}