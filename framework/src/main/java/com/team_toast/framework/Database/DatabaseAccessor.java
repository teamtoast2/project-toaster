package com.team_toast.framework.Database;

import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by qrk13cdu on 27/11/2015.
 */
public class DatabaseAccessor extends AsyncTask<String, Void, JSONArray>  {

    /**
     * doInBackground creates a connection with the database, executes a query
     * and returns results in a jsonarray.
     * @param params Array of string parameters. params[0] is the given query
     * @return JSONArray, containing results from the query accessible by coloumn name and row index
     */
    @Override
    protected JSONArray doInBackground(String... params) {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://10.0.2.2:5432/studentdb",
                    "student",
                    "dbpassword"
            );

        } catch (Exception e) {
            System.out.println("Joe Error  " + e);

        }
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    params[0]);
            //executes query
            ResultSet resultSet = pstmt.executeQuery();
            JSONArray jsonArray = new JSONArray();

            //puts all results in a json array
            while (resultSet.next()) {
                int total_rows = resultSet.getMetaData().getColumnCount();
                JSONObject obj = new JSONObject();
                for (int i = 0; i < total_rows; i++) {
                    obj.put(resultSet.getMetaData().getColumnLabel(i + 1)
                            .toLowerCase(), resultSet.getObject(i + 1));
                }
                jsonArray.put(obj);
            }
            connection.close();
            return jsonArray;
        } catch (Exception ex) {
        }
        return null;
    }
}
