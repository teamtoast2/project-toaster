package com.team_toast.framework.Location;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.team_toast.framework.AppManagement.Permissions;

/**
 * Superclass to represent a location object. Utilises strategy design pattern.
 */
public abstract class Location {

    protected LatLng coordinate;
    protected LocationManager locationManager;
    protected String provider;
    public boolean isnull;

    public Location(){
        //initialise
        coordinate = new LatLng(0,0);
        this.isnull=true;
    }

    public Location(Context currentContext){
        //permissions check
        Permissions permissions = new Permissions();
        permissions.requestFinePermissions(currentContext);
        permissions.requestCoarsePermissions(currentContext);
    }

    public Location(Context currentContext, LatLng latLng){
        Permissions permissions = new Permissions();
        permissions.requestFinePermissions(currentContext);
        permissions.requestCoarsePermissions(currentContext);
        coordinate = latLng;
    }

    /**
     * Anonymous inner class to create a LocationListener object
     */
    protected LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(android.location.Location location) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };

    /**
     * Initialises LocationManager
     * @param currentContext
     */
    public void initialiseLocationManager(Context currentContext){
    }

    /**
     * Updates Location object to devices last known location.
     */
    public void updateToLastKnownLocation(){
        android.location.Location location = null;
        try{
           // Log.e("Location","About to get Location");
            location = locationManager.getLastKnownLocation(provider);
        }catch(Exception e) {
            String err=e.toString();
            //Log.e("Location", err);
        }
        isnull = location == null ? true : false;
        if(!isnull) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            coordinate = new LatLng(lat, lng);
        }
    }

    /**
     * Accessor method
     * @return current coordinates
     */
    public LatLng getLatLng(){
        return coordinate;
    }

    /**
     * Setter method
     * @param newLatitude New latitude to use
     */
    public void setLatitude(float newLatitude){
        coordinate = new LatLng(newLatitude, this.coordinate.longitude);
    }

    /**
     * Setter method
     * @param newLongitude New Longitude to use
     */
    public void setLongitude(float newLongitude){
        coordinate = new LatLng(this.coordinate.latitude, newLongitude);
    }

}
