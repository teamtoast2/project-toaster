package com.team_toast.framework.Location;

import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.team_toast.framework.Location.Location;

/**
 * Class to use cellular or wifi based location
 */
public class AndroidCellularLocation extends Location {

    public AndroidCellularLocation(){
        //initialise
        coordinate = new LatLng(0,0);
    }

    public AndroidCellularLocation(Context currentContext){
        super();
    }

    /**
     * Initialise the location manager to use a cellular or wifi based location
     * @param currentContext
     */
    public void initialiseLocationManager(Context currentContext){
        Criteria criteria = new Criteria();
        locationManager = (LocationManager) currentContext.getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(criteria, true);

        for (String s : locationManager.getAllProviders()){
           // Log.w("LocationProviders", s);
        }
        try{
            locationManager.requestLocationUpdates("network", (long) 5000, 20.0f, locationListener);
        }catch(Exception e) {
            String err=e.toString();
            //Log.e("Location", err);
        }
    }

}
