package com.team_toast.framework.DataInterchange;

import android.test.AndroidTestCase;

import junit.framework.TestCase;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

public class AndroidXmlParserTest extends AndroidTestCase {

    public void testLoadFile() throws Exception {

        AndroidXmlParser testParser = new AndroidXmlParser(getContext()) {
            @Override
            public void parseData() throws XmlPullParserException, IOException {

                int eventType = parser.getEventType();

                while (eventType != XmlPullParser.END_DOCUMENT) {
                    String name = null;
                    switch (eventType) {
                        case XmlPullParser.START_DOCUMENT:
                            break;
                        case XmlPullParser.START_TAG:
                            name = parser.getName();
                            assertEquals("testcase", name);
                            break;
                        case XmlPullParser.END_TAG:
                            name = parser.getName();
                    }
                    eventType = parser.next();
                }
            }
        };

        testParser.loadFile("testcase.xml");
    }
}