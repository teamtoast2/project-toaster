package com.team_toast.framework.Maps;

import junit.framework.TestCase;

public class GoogleMapDirectionsTest extends TestCase {

   /**
    * Test to ensure the correct URL is being created within the method
    * @throws Exception
    */
    public void testMakeURL() throws Exception {
        GoogleMapDirections testGoogleMapDirections = new GoogleMapDirections();
        String testURL = testGoogleMapDirections.makeURL(50,51,52,53);
        String ExpectedUrl =
                "https://maps.googleapis.com/maps/api/directions/json?origin=50.0,51.0&destination=" +
                        "52.0,53.0&sensor=false&mode=walking&alternatives=true&key=AIzaSyCI84NQq8mKr6ojMiYtxv2lBgRiPr-LAME";
        assertEquals(ExpectedUrl, testURL);
    }

   /**
    * Test to ensure the correct JSON data is being retrieved.
    * @throws Exception
    */
    public void testGetJSONFromUrl() throws Exception {
        GoogleMapDirections testGoogleMapDirections = new GoogleMapDirections();
        String testJSON = testGoogleMapDirections.getJSONFromUrl(52.62323666666666,1.2427716666666666,52.622108459472656,1.2406150102615356);
        String expectedJSON = "{\n" +
                "   \"geocoded_waypoints\" : [\n" +
                "      {\n" +
                "         \"geocoder_status\" : \"OK\",\n" +
                "         \"place_id\" : \"ChIJ2S1EziXh2UcRi2FC92ZKlX4\",\n" +
                "         \"types\" : [ \"route\" ]\n" +
                "      },\n" +
                "      {\n" +
                "         \"geocoder_status\" : \"OK\",\n" +
                "         \"place_id\" : \"ChIJ7Zz2Dibh2UcRkfHyrDlKmLk\",\n" +
                "         \"types\" : [ \"premise\" ]\n" +
                "      }\n" +
                "   ],\n" +
                "   \"routes\" : [\n" +
                "      {\n" +
                "         \"bounds\" : {\n" +
                "            \"northeast\" : {\n" +
                "               \"lat\" : 52.6232399,\n" +
                "               \"lng\" : 1.2428525\n" +
                "            },\n" +
                "            \"southwest\" : {\n" +
                "               \"lat\" : 52.6220837,\n" +
                "               \"lng\" : 1.2407921\n" +
                "            }\n" +
                "         },\n" +
                "         \"copyrights\" : \"Map data Â©2016 Google\",\n" +
                "         \"legs\" : [\n" +
                "            {\n" +
                "               \"distance\" : {\n" +
                "                  \"text\" : \"0.3 km\",\n" +
                "                  \"value\" : 267\n" +
                "               },\n" +
                "               \"duration\" : {\n" +
                "                  \"text\" : \"3 mins\",\n" +
                "                  \"value\" : 186\n" +
                "               },\n" +
                "               \"end_address\" : \"University Plain, Norwich, Norfolk NR4 7AF, UK\",\n" +
                "               \"end_location\" : {\n" +
                "                  \"lat\" : 52.6220837,\n" +
                "                  \"lng\" : 1.2408087\n" +
                "               },\n" +
                "               \"start_address\" : \"Cow Dr, Norwich, Norfolk NR4 7SS, UK\",\n" +
                "               \"start_location\" : {\n" +
                "                  \"lat\" : 52.6232399,\n" +
                "                  \"lng\" : 1.2428157\n" +
                "               },\n" +
                "               \"steps\" : [\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"21 m\",\n" +
                "                        \"value\" : 21\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 15\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6230506,\n" +
                "                        \"lng\" : 1.2428525\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Head \\u003cb\\u003esouth\\u003c/b\\u003e\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"g~d`IsvqFd@E\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6232399,\n" +
                "                        \"lng\" : 1.2428157\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"0.1 km\",\n" +
                "                        \"value\" : 113\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 80\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6229843,\n" +
                "                        \"lng\" : 1.2411992\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-right\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"a}d`IyvqF@X?NB^?FBj@@h@D|A@L?JAJAPAJ\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6230506,\n" +
                "                        \"lng\" : 1.2428525\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"15 m\",\n" +
                "                        \"value\" : 15\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 11\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6228537,\n" +
                "                        \"lng\" : 1.2412415\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eleft\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-left\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"s|d`IolqFF?BADAFC\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6229843,\n" +
                "                        \"lng\" : 1.2411992\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"46 m\",\n" +
                "                        \"value\" : 46\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 31\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6225469,\n" +
                "                        \"lng\" : 1.2407921\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e at \\u003cb\\u003eUniversity Dr\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-right\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"y{d`IwlqF\\\\d@JRP^\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6228537,\n" +
                "                        \"lng\" : 1.2412415\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"34 m\",\n" +
                "                        \"value\" : 34\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 25\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6223463,\n" +
                "                        \"lng\" : 1.2411734\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eleft\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-left\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"}yd`I}iqFf@kA\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6225469,\n" +
                "                        \"lng\" : 1.2407921\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"38 m\",\n" +
                "                        \"value\" : 38\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 24\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6220837,\n" +
                "                        \"lng\" : 1.2408087\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-right\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"uxd`IilqFt@fA\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6223463,\n" +
                "                        \"lng\" : 1.2411734\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  }\n" +
                "               ],\n" +
                "               \"via_waypoint\" : []\n" +
                "            }\n" +
                "         ],\n" +
                "         \"overview_polyline\" : {\n" +
                "            \"points\" : \"g~d`IsvqFd@E@XBn@JzD@XC\\\\AJF?HCFC\\\\d@\\\\r@f@kAt@fA\"\n" +
                "         },\n" +
                "         \"summary\" : \"\",\n" +
                "         \"warnings\" : [\n" +
                "            \"Walking directions are in beta.    Use caution â\u0080\u0093 This route may be missing sidewalks or pedestrian paths.\"\n" +
                "         ],\n" +
                "         \"waypoint_order\" : []\n" +
                "      },\n" +
                "      {\n" +
                "         \"bounds\" : {\n" +
                "            \"northeast\" : {\n" +
                "               \"lat\" : 52.6232399,\n" +
                "               \"lng\" : 1.2430283\n" +
                "            },\n" +
                "            \"southwest\" : {\n" +
                "               \"lat\" : 52.6220837,\n" +
                "               \"lng\" : 1.2408087\n" +
                "            }\n" +
                "         },\n" +
                "         \"copyrights\" : \"Map data Â©2016 Google\",\n" +
                "         \"legs\" : [\n" +
                "            {\n" +
                "               \"distance\" : {\n" +
                "                  \"text\" : \"0.3 km\",\n" +
                "                  \"value\" : 289\n" +
                "               },\n" +
                "               \"duration\" : {\n" +
                "                  \"text\" : \"3 mins\",\n" +
                "                  \"value\" : 204\n" +
                "               },\n" +
                "               \"end_address\" : \"University Plain, Norwich, Norfolk NR4 7AF, UK\",\n" +
                "               \"end_location\" : {\n" +
                "                  \"lat\" : 52.6220837,\n" +
                "                  \"lng\" : 1.2408087\n" +
                "               },\n" +
                "               \"start_address\" : \"Cow Dr, Norwich, Norfolk NR4 7SS, UK\",\n" +
                "               \"start_location\" : {\n" +
                "                  \"lat\" : 52.6232399,\n" +
                "                  \"lng\" : 1.2428157\n" +
                "               },\n" +
                "               \"steps\" : [\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"21 m\",\n" +
                "                        \"value\" : 21\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 15\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6230506,\n" +
                "                        \"lng\" : 1.2428525\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Head \\u003cb\\u003esouth\\u003c/b\\u003e\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"g~d`IsvqFd@E\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6232399,\n" +
                "                        \"lng\" : 1.2428157\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"25 m\",\n" +
                "                        \"value\" : 25\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 18\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6230234,\n" +
                "                        \"lng\" : 1.2424776\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e toward \\u003cb\\u003eUniversity Dr\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-right\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"a}d`IyvqF@X?NB^\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6230506,\n" +
                "                        \"lng\" : 1.2428525\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"55 m\",\n" +
                "                        \"value\" : 55\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 39\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6227689,\n" +
                "                        \"lng\" : 1.2430283\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eleft\\u003c/b\\u003e toward \\u003cb\\u003eUniversity Dr\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-left\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"{|d`IotqFNAHABCBCBC@G@I@K?ODo@\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6230234,\n" +
                "                        \"lng\" : 1.2424776\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"0.1 km\",\n" +
                "                        \"value\" : 95\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 70\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.62273640000001,\n" +
                "                        \"lng\" : 1.2416385\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Sharp \\u003cb\\u003eright\\u003c/b\\u003e onto \\u003cb\\u003eUniversity Dr\\u003c/b\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-sharp-right\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"i{d`I}wqF@J@T@LBPA\\\\?d@BnA?RAP?JCL\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.6227689,\n" +
                "                        \"lng\" : 1.2430283\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"distance\" : {\n" +
                "                        \"text\" : \"93 m\",\n" +
                "                        \"value\" : 93\n" +
                "                     },\n" +
                "                     \"duration\" : {\n" +
                "                        \"text\" : \"1 min\",\n" +
                "                        \"value\" : 62\n" +
                "                     },\n" +
                "                     \"end_location\" : {\n" +
                "                        \"lat\" : 52.6220837,\n" +
                "                        \"lng\" : 1.2408087\n" +
                "                     },\n" +
                "                     \"html_instructions\" : \"Turn \\u003cb\\u003eleft\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003eDestination will be on the left\\u003c/div\\u003e\",\n" +
                "                     \"maneuver\" : \"turn-left\",\n" +
                "                     \"polyline\" : {\n" +
                "                        \"points\" : \"c{d`IgoqFJJDHHJV^PZ@@@?@A@?t@fA\"\n" +
                "                     },\n" +
                "                     \"start_location\" : {\n" +
                "                        \"lat\" : 52.62273640000001,\n" +
                "                        \"lng\" : 1.2416385\n" +
                "                     },\n" +
                "                     \"travel_mode\" : \"WALKING\"\n" +
                "                  }\n" +
                "               ],\n" +
                "               \"via_waypoint\" : [\n" +
                "                  {\n" +
                "                     \"location\" : {\n" +
                "                        \"lat\" : 52.622813,\n" +
                "                        \"lng\" : 1.2426282\n" +
                "                     },\n" +
                "                     \"step_index\" : 2,\n" +
                "                     \"step_interpolation\" : 0.5\n" +
                "                  }\n" +
                "               ]\n" +
                "            }\n" +
                "         ],\n" +
                "         \"overview_polyline\" : {\n" +
                "            \"points\" : \"g~d`IsvqFd@E@XBn@XCFGDKBUD_AB`@D^AbABbBA\\\\CLJJNTj@|@DAt@fA\"\n" +
                "         },\n" +
                "         \"summary\" : \"University Dr\",\n" +
                "         \"warnings\" : [\n" +
                "            \"Walking directions are in beta.    Use caution â\u0080\u0093 This route may be missing sidewalks or pedestrian paths.\"\n" +
                "         ],\n" +
                "         \"waypoint_order\" : []\n" +
                "      }\n" +
                "   ],\n" +
                "   \"status\" : \"OK\"\n" +
                "}\n";

        assertEquals(expectedJSON, testJSON);

    }
}