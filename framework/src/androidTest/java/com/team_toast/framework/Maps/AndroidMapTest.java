package com.team_toast.framework.Maps;

import com.google.android.gms.maps.model.LatLng;

import junit.framework.TestCase;

import java.util.List;

public class AndroidMapTest extends TestCase {

    AndroidMap testMap;

    /**
     * Test to see if the singleton design pattern is correctly creating an instance of AndroidMap
     * @throws Exception
     */
    public void testGetInstance() throws Exception {
        testMap = AndroidMap.getInstance();
        assertNotNull(testMap);
    }

}