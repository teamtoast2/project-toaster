package com.team_toast.framework.FileIO;

import android.accounts.AccountAuthenticatorActivity;
import android.app.Activity;
import android.test.AndroidTestCase;

import junit.framework.TestCase;

/**
 * Created by qrk13cdu on 13/01/2016.
 */
public class SimpleTextTest extends TestCase{
    SimpleText test = new SimpleText(new InternalFileImp(new Activity()));
    public void testSetMyStrat() throws Exception {

    }

    public void testSetBody() throws Exception {
        test.setBody("content");
        assertEquals("content", test.formatOutput());

    }

    public void testFormatOutput() throws Exception {
        assertEquals("content", test.formatOutput());
    }

    public void testWriteToFile() throws Exception {
        assertEquals(true, test.writeToFile("test.test"));
    }

    public void testReadFromFile() throws Exception {
        SimpleText test = new SimpleText(new InternalFileImp(new Activity()));
        test.readFromFile("test.test");
        assertEquals("content",test.formatOutput());
    }
}